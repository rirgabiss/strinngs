# STRinNGS version 2.1

STRinNGS is a tool to analyse Short Tandem Repeats (STR) in massive parallel sequencing data (MPS) also known as Next Generation Sequencing (NGS). It analyses both the STR and the flanking regions, and names the alleles according to the STRidER guidelines as well as an in-house nomenclature that also include variants in the flanking regions.


## Installation

#### Docker 

```bash
docker pull bioinformatician/strinngs2
```

See https://hub.docker.com/r/bioinformatician/strinngs2 for further information.

#### Linux: installation from source

The following external dependencies are needed to run STRinNGS v2.1:

```
python 3.6 including pip
samtools version 1.9
hisat2 version 2.1.0
stringtie version 2.0.3
agrep T.R.E. version 0.8.0
```

To install STRinNGS:

```
# Install Python packages
wget https://bitbucket.org/rirgabiss/strinngs/downloads/requirements.txt
pip install -r requirements.txt

# Install STRinNGS v2.1
wget https://bitbucket.org/rirgabiss/strinngs/downloads/strinngs_v2.1.tar.gz
tar -xvf strinngs_v2.1.tar.gz && rm strinngs_v2.1.tar.gz
cd strinngs_v2.1

```

#### Test set

A test set is available to test the functionality of the program.
It contains everything needed to test the program: a reference for chromosome 12 on hg38 (chr12.fa), the matching index file (chr12.fa.fai), a fastq.gz (test.fastq.gz) and a configuration file (test.ini). The test set also contains a folder with the expected results: a log file, results.csv and raw_results.csv. The test set is ~290MB unzipped. 

The test set is available from Bitbucket:
https://bitbucket.org/rirgabiss/strinngs/ or download directly as:

```
wget https://bitbucket.org/rirgabiss/strinngs/downloads/test_set.tar.gz
tar -xvf test_set.tar.gz && rm test_set.tar.gz
```


## Usage


#### To run

```
./src/STRinNGS.py -i test_set/test.fastq.gz -o ./results/test -r test_set/chr12.fa -n test_set/test.ini [optional]
```

#### Options available

```bash
-h, --help       Show options
--version        Show version

Required:
-i, --input      Input file. Must be BAM or FASTQ format
-o, --outdir     Name of output folder
-r, --ref        Reference genome FASTA file
-n, --nom        Configuration file in .ini format

Optional:
-m, --mism       Number of mismatches to add to ini mismatch [Default: 0]
-t, --threads    number of cores available for multi processing [Default: 1]
-w, --women      Maximum number of reads to identify females [Default: 25]
-s, --snpcheck   VCF file containing known SNPs [Default: None]
-d, --dir        If only a directory is given, state file type [Default: None]
-f, --force      Overwrite existing output file if it exists
```


#### Command line parameters:

##### Required parameters:

**--input, -i:**
See section on "Input files" for further information.

**--outdir, -o:**
Name and path to outdir. Partial or full path may be used.

**--ref, -r:**
Reference genome as a FASTA file.

**--nom, -n:**
See section of "Configuration file specification" for further information.


##### Optional parameters:

**--mism, -m:** 
Number of mismatches to <ins>add</ins> to all ini mismatch [Default: 0]. Use this when testing a new population, instead of adjusting the configuration file. Be aware that this will increase run time.

**--threads, -t:** 
Number of cores available for multi processing [Default: 1]. It is recommended to have as many threads as loci.

**--women, -w:** 
Maximum number of reads to identify females [Default: 25].

**--snpcheck, -s:** 
VCF file containing known SNPs [Default: None]. If a locus does not have rs_info in the configuration file, this parameter will check the given vcf file for rs numbers for found SNP positions. New rs numbers will be added to log. Be aware that this will increase run time. 

**--dir, -d:** 
If only a directory is given, state file type [Default: None]. This will search the input folder for a specific type of files. See section of "Input files" for further information.

**--force, -f:** 
Overwrite existing output file even if it exists.


#### Input files

FASTA and FASTQ files containing one sample or SAM, BAM or CRAM files containing one or more samples identified inside the file by their ID and/or SN (SampleName) under the RG (ReadGroup). 
Multiple files may be analyzed in the same run. If multiple samples are analyzed, the file type must be identified by using the -d/--dir option. If so, please give a *directory* as input (-i), and write the type of file that the program should search for eg.:
    
    -d fastq
    
Following file types are supported:

    fasta
    fastq
    sam
    bam
    cram

Bam and cram files needs index files. The file *must* be named: sample.bam.bai or sample.cram.crai. 

Zipped files (.gz) will automatically be checked, it does not need to be specified.

Paired end reads are not supported.



#### Output files

**Results.csv:**
The final results after noise removal, added comments and calculations. Separated by comma.

**Raw_results.csv:**
All information except Locus Balance, Heterozygote Balance and STRidER nomenclature. Separated by comma.

**Log_date_time.txt:**
Information about the run.



#### Configuration file specifications

In order to run STRinNGS, a configuration file (.ini file) must be created, specifying information of each loci.
The configuration file contains 4 mandatory parameters (name, start, stop, chr) and 18 optional parameters for each locus. A configuration file can be created like this:

```bash
[locus1]
start = 123456
stop = 123499
chr = 4

[locus2]
start = 456789
stop = 456810
chr = 7
```

The locus name will be the separator for each locus. The order of the parameters within each locus does not matter. The file must have unix line endings, blank lines are accepted and lines starting with ; will be viewed as comments.

##### Mandatory parameters:

**name:**
Specify the name of the locus inside the square brackets. Each name may only appear once in the the ini file, and all parameters for this locus should be placed below the name. All characters, except ] but including spaces, in the name are possible, but only letters, numbers, underscores and hyphens are recommended.

**start:**
The last position *before* the STR begins.

**stop:**
The last position *in* the STR.

**chr:**
Name of the chromosome the locus is placed upon. Note that chromosome variable must match reference genome, so if your reference looks like >1 then your ini file should state chr = 1. If your reference looks like >chr1 then your ini file should state chr = chr1

##### Optional parameters:


**flank_up_length and flank_down_length:**
The number of bases on either side of the STR, that is used to identify the locus and are analyzed for variants. Up and down flank is always located before and after the STR respectively in the reference genome, no matter the strand. Default value = 25. <br>
Minus flanks should only be used for AMEX og AMELY. Minus flanks can only be used with both flanks in minus, and ignore_pos is not available in this area.

**mism_up and mism_down:**
The number of mismatches allowed for the specific flank. This includes substitutions, insertions, and deletions. This is direction specific, so if the reads direction is reverse, then the mismatch number in the down flank should be in the mism_up parameter. Direction can be found in the raw_data.csv file. If it is not known in which direction the STR is sequenced, it is recommended that mism_up and_down both contains the same number of mismatches. *WARNING:* Runtime is affected exponentionally by the number of mismatches, so it is highly recommended to keep mismatches as low as possible.
It is also possible to add mismatches from the command line when running the script, but this will be added to ALL flanks in the configuration file. Due to the runtime, it is recommended to use this parameter only when testing a new population. Default value = 2

**unit_length:**
The length of the sub-repeat. This will be used to calculate the CE allele name (PredAllele). Default value = 4

**subtract:**
CE name (PredAllele) is calculated as the number of bases in the STR region (start to stop) divided by unit_length. If a locus does not match this pattern, subtract can be specified in the configuration file as number of sub-repeats followed by a dot and then the number of single bases. eg. 2.1 will *subtract* 2 sub-units and 1 base when calculating the PredAllele. -2.1 will *add* 2 sub-units and 1 base when calculating the PredAllele. Default value = 0.0

**strand:**
Strand specifies the direction of the given nomenclature, where forward (f) is the same as the reference genome. Only two options are possible: f or r for forward and reverse respectively.
If reverse (r) is chosen, notice that flank_up_match, flank_down_match and STR all will be reverse complimentary to the reference genome, and the flanks will have switched positions. Default value = f. <br>
Reverse is depreciated from version 2.1.


**nomenclature:**
Specify the nomenclature using capital letters. The following numericals can be used: 

    [*] : the specified nomenclature must occur 0 or more times 
    [?] : the specified nomenclature must occur 0 or 1 times 
    [+] : the specified nomenclature must occur 1 or more times
    [3] : the specified nomenclature must occur exactly 3 times 
   
    N[n] : An identified number of bases (minimum 3) that does not match the other nomenclature. 

New line indicates new nomenclature. Do not use comma or other separator.
Spaces in front of nomenclature will be ignored. 
example: 

```
nomenclature = 	AGAT[+]
                AGAT[+]ACAT[1]AGAT[+]
```


If the sequence of the STR region is not recognized as a known allele, a warning flag (Allele not defined) is shown in the result file. Furthermore, the bases that were not parsed correctly are shown in lower case in the ReportedNom column of the result file.

Default value = empty string


**rs_info:**
Rs info in flanks.  Info must be chromosome:position:rsname:ref & alt allele.
Note that chromosome variable must match reference genome, so if your reference looks like >1 then your ini file should 1. If your reference looks like >chr1 then your ini file should state chr1.
Multiple rs numbers can be separated by comma. 

example: 

    rs_info = chr5:123111246:rs73801920:GT,chr5:123111306:rs25768:TC 

For substitutions rs info alternate base will be matched to found base. If it does not match, or if it's a deletion or insertion a flag: "SNP allele not defined" will be raised. Default value = empty string

**ignore_pos:**
Ignore position will replace bases in the flank with N unless it is an insertion 
then it will not show any base. Any mismatch whether insertion, deletion or 
substitution will be ignored, if the position is specified in the configuration 
file. The purpose is to collapse sequences to one unique sequence despite 
mismatches in the specified positions.
Single positions can contain insertions: 86386300.1T. Notice that only the base 
indicated (here T) will be ignored.
For intervals, both start and stop numbers are replaced by N. 
Intervals must always be integers, insertions cannot be given as intervals.
A single position or an interval of positions can be given like this: 

example 1 (this will ignore 2 single positions where the last one is an insertion):

    ignore_pos = 123456789,86386300.1T  
    
example 2 (this will ignore a single position *and* an interval for same locus): 

    ignore_pos = 123456789,123456792-123456799 

When finding positions to ignore, be aware that alignments might differ:

    Multiple alignments can have the same score, and for insertions the 
    position might change due to the number of inserts:

    # Single insertion (insertion will be at the END of the poly chain)
    'AGAGGTATAAA-TAAGGATACAGATAAAGATACAAATGTTGT'
    'AGAGGTATAAAATAAGGATACAGATAAAGATACAAATGTTGT'

    # multiple insertion (insertion will be at the BEGINNING of the poly chain)
    'AGAGGTAT--AAATAAGGATACAGATAAAGATACAAATGTTGT'
    'AGAGGTATAAAAATAAGGATACAGATAAAGATACAAATGTTGT'

    Biopythons pairwise2.align.localms used for the comparison alignment is consistent in this.

Default value = empty string

**noise_filter:**
Noise threshold. All unique sequences with lower read depth (in percent of total reads for locus) than the noise threshold are identified and removed from downstream analyses. Default value: 0.01

**min_reads:**
Threshold for minimum read depth. After the noise reads are removed, the remaining number of reads (reads for genotype calling) must exceed the threshold for minimum read depth. If not, no alleles are identified and a warning flag is indicated in the result file:  Locus dropout if the read depth is 0 and Too few reads if the read depth is between 1 and min_reads. Default value = 100

**max_num_unique:**
Expected number of unique sequences in the result file. After the noise reads are removed, the remaining reads should constitute a low number of unique sequences (primarily alleles and stutters). If the number of unique sequences exceeds the expected number, a warning flag (Many unique sequences) is indicated in the result file. Default value is 4 for Y-STRs and 6 for X-STRs and autosomal STRs. Default value = 6

**min_frac_genotype:**
Minimum threshold for genotype calling. After the noise reads are removed, the true alleles should make up a large fraction of the remaining reads (reads for genotype calling). If the reads of the unique sequence with most reads exceed the threshold for genotype calling, only one allele is identified and the individual is assumed to be homozygous. If the most numerous sequence has fewer reads than required, STRinNGS sum up the reads of the two most frequent sequences. If the sum exceeds the threshold for genotype calling, two alleles are identified and the individual is assumed to be heterozygous. If the sum do not exceed the threshold, STRinNGS calculates the sum of the three most frequent sequences. If this sum exceeds the threshold, three alleles are identified and a warning flag (Three alleles) is indicated in the result file. If four or more unique sequences are necessary to exceed the threshold for genotype calling, no alleles are identified and a warning flag (More than three alleles) is shown in the result file. Default value = 0.7

**min_frac_profile:**
Test of locus balance (Lb). Lb = read depth of called allele(s) in a locus/read depth of all called alleles for the sample. If Lb < min_frac_profile, a warning flag (Low fraction of reads) in the result file indicates that the number of reads for this locus is relatively low. Default value = 1/1.5*number of loci in the assay. 


**hetero_balance:**
Test of heterozygote balance (Hb). Hb = read depth of the shortest allele/read depth for both alleles. If the two alleles have the same size, Hb = read depth of the allele with most reads/read depth for both alleles. If Hb is outside the acceptable range, a warning flag (Heterozygote imbalance) is shown in the result file. Default range = 0.25-0.75

**max_reads_unique_not_called:**
Maximum read depth (in percent of the genotype read depth) for unique sequences that are not identified as noise or true alleles. The value S = read depth of unique sequence/read depth of genotype is calculated for each unique sequence that is not identified as noise or a true allele. If S>max_reads_unique_not_called, a warning flag (Sequence with many reads not called) is shown in the result file. This threshold is often identical to the maximum stutter ratio and may be considered a stutter acceptance criterion. Default value M = 0.1 (homozygous genotypes), M/2 (heterozygous genotypes), and M/3 (tri-allelic genotypes). Default value = 0.1

**min_unex:**
Minimum threshold for unique sequences that are not identified as noise, true alleles, or n-1 stutters. A warning flag (Unexpected sequence detected) is indicated in the result file if a unique sequence that is not identified as noise, a true allele, or a n-1 stutter has a read depth>min_unex. Default value = 15


#### Example of locus with all parameters altered:

```bash
[FictiveLocus]
start = 456789
stop = 456810
chr = chr7

flank_up_length = 123
flank_down_length = 45
mism_up = 3
mism_down = 3
unit_length = 3
subtract = 1.2
strand = r
rs_info = chr7:456794:rs111:GT,chr7:456800:rs222:TC
ignore_pos = 456782,456817.1T
noise_filter = 0.02
min_reads = 80
max_num_unique = 8
min_frac_genotype = 0.8
min_frac_profile = 0.1
hetero_balance = 0.20-0.8
max_reads_unique_not_called = 0.15
min_unex = 20

nomenclature = 	AGAT[+]
                    AGAT[+]ACAT[1]AGAT[+]
```



### Post analysis

As as post analysis it is possible to generate files for overview and for STRidER submission.

#### To run

```bash
genotypes -i /path/input -o /path/output [optional]
```


#### Options available

```bash

-h, --help    Show this screen.
--version     Show version.

Required options:
-i, --input   Input file. Must be excel OR csv format
-o, --outdir  Name of output folder.

Optional options:
--both        Use run AND sample names to get unique ID. Will be separated with |.
--create      Create sequence files for STRidER

```

#### Input file


Two types of input files are possible: csv or an excel file.
The only requirement is that the following columns must be present:
['run', 'sample', 'locus', 'PredAllele', 'Comment'], and that 'run' and
'sample' are in the first line and after each other, as this is used to determine
the separator for the csv file. Notice that column names are *case sensitive*.


#### Sample Dropout: 
Sample Dropout catches samples where no sequences were found for any loci. There will be one line in the results.csv for each locus for each sample, where "Sample Dropout" will be placed in in all columns except run, sample and locus. 
They will not appear in the STRidER files.


#### Female identification:
STRinNGS version 2.1 will identify female samples for the Y chromosome. 
This is done by finding the sum of counts for each Y loci in a sample (after noise). If ALL sums are below or equal to a given number [default is 25], then all Y loci will be identified as "Female". "Female" will then be written in each column in the results file except run, sample, locus and count.

Females will appear differently in the different STRidER files:

    AUTHOR_COUNTRY_x.txt: Females will appear as a hyphen - (unlike "Locus Dropout" and "Too Few Reads" that appear as "nd")
    
    AUTHOR_COUNTRYCODE_x_locus.txt: Identified Females have been removed for Y loci, and x has been adjusted accordingly.
    
    results_overview_w_comments.txt: Females will appear as "Female"
    
The default can be changed on the command line using the -w / --women option that has been added. This is NOT locus specific.




#### Output files

This program will create minimum two files containing genotypes for each sample: 

**The first file** is a STRidER_STR_DATASET.txt file containing all genotypes
(CE alleles) for each sample. The output file should later be renamed according
to STRidER's specifications, and the header should be filled in manually. In
homozygotes, allele 1 have been duplicated in order to meet STRidER specifications.
WARNING:Tri-alleles will only be shown with the two alleles with most reads.
On-screen warnings will issued for tri-alleles and up.

**The second file** is the same as above, but the header is missing, and if the allele originally had a comment, a star (*) will be placed next to it. The file will be named results_overview_w_comments.txt and is intended for the scientists to have an overview over all samples/genotypes, including genotypes with comments. In homozygotes the second allele will be replaced with '-'. In tri-alleles only the first two alleles will be shown, and both will be marked with a 'T' after the allele.

If no allele was found, or if the count was considered too low, for a
sample/locus 'nd' (not determined) will be inserted instead, in order to keep
the order of the alleles. This applies to all output files.


'Sample ID' contains the name from the 'run'. If this is not enough in order to
create unique sample ID, then the parameter --both can be added, and then both
run AND sample will be used as 'Sample ID'.

**Optional files**

If --create is added to the command line, the program will generate sequence
files for STRidER. One file for each locus, containing two columns:
samplename_locus_CEallele tab-separation flankupSTRflankdown eg.:

    >sample1_TPOX_8 CTTAGGGAACCCTCACTGAATGAATGAATGAATGAATGAATGAATGAATGTTTGGGCAAATAAACGCTGACAAG

Notice that homozygotes will be shown twice, and all three alleles of triallelic genotypes  will be shown.
The files will be pre-named AUTHOR_COUNTRYCODE_samplenumber_locus.txt.
Capital letters should be replaced manually, according to STRidER specifications.

Output folder will be overwritten if it exists, or created if it does not exist
beforehand.

## Changes since version 2.0

Following updates have been made since version 2.0: 1) Sample dropout has been added to catch samples where no sequences were found at all. There will be one line in the results.csv for each locus for each sample, where "Sample Dropout" will be placed in all columns except run, sample, and locus. They will not appear in the STRidER files. 2) STRinNGS v2.1 will identify female samples for the Y chromosome. This is done by finding the sum of counts for each Y-STR in a sample (after noise). If all sums are below or equal to a given number (default is 25, not locus specific), then all Y-STRs will be identified as "Female". "Female" will be written in each column in the results file except run, sample, locus and count. "Female" will appear differently in the different STRidER files. The default can be changed on the command line using the -w / --women option that has been added. 3) Speed optimization. In order to reduce run time, STRinNGS v2.1 no longer name or find variations for sequences below noise level, as noise removal has been moved to an earlier step. 4) Better mapping. In order to improve the SNP and indel identification, in the flanks, open gap penalty was reduced from -4 to -3. 5) Errors fixed. Intervals for ignore_pos was not functioning; position error where an insertion after ignore_pos in up flank. 6) STRinNGS version has been added to the log file. 7) The program now tests that the rs-positions are within the defined flanks.

## Changes since version 1.0

STRinNGS version 2.0 is a refactored version of STRinNGS version 1.0 [1] with additional features. The configuration file was changed to an ini file format, to ease the overview of each locus. In STRinNGS version 2.0, the configuration file accepts an unlimited number of loci and rs numbers in the flanks. The old criteria were renamed to be more intuitive, the criteria DIR and SH were removed, and 11 new criteria for genotyping were implemented. 

## Author

Developed by: 

    BioInformatic Science Support (BISS)
    Copenhagen University
    Faculty of Health Sciences
    Department of Forensic Medicine
    Section of Forensic Genetics
    https://retsmedicin.ku.dk/english/
               
## Support

For comments, suggestions and support please contact Carina J�nck (carina.joenck@sund.ku.dk)

## License and citation

License can be found at:
https://bitbucket.org/rirgabiss/strinngs/src/master/LICENSE.md

Please cite STRinNGS v2.1 as:

	C.G. J�nck, X. Qian, H. Simayijiang, C. B�rsting, STRinNGS v2.0: Improved tool for analysis and reporting of STR sequencing data. Forensic Sci. Int. Genet. 48 (2020). doi: 10.1016/j.fsigen.2020.102331.


## References

[1] S.L. Friis, A. Buchard, E. Rockenbauer, C. B�rsting, N. Morling, Introduction of the Python script STRinNGS for analysis of STR regions in FASTQ or BAM files and expansion of the Danish STR sequence database to 11 STRs. Forensic Sci. Int. Genet. 21 (2016) 68-75. doi: 10.1016/j.fsigen.2015.12.006.
